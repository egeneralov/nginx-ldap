ARG IMAGE=debian:11

FROM ${IMAGE}

RUN \
  apt-get update -q && \
  apt-get install -yq build-essential pkg-config checkinstall wget ca-certificates git && \
  apt-get clean -yq

RUN \
  wget https://ftp.exim.org/pub/pcre/pcre-8.43.tar.gz && \
  tar -zxf pcre-8.43.tar.gz && \
  cd pcre-8.43 && \
  ./configure && \
  make && \
  make install && \
  checkinstall -y

RUN \
  wget https://zlib.net/fossils/zlib-1.2.11.tar.gz && \
  tar -zxf zlib-1.2.11.tar.gz && \
  cd zlib-1.2.11 && \
  ./configure && \
  make && \
  make install && \
  checkinstall -y

RUN apt-get install -yq libssl-dev
#RUN \
#  wget http://www.openssl.org/source/openssl-1.1.1c.tar.gz && \
#  tar -zxf openssl-1.1.1c.tar.gz && \
#  cd openssl-1.1.1c && \
#  ./Configure linux-x86_64 --prefix=/usr && \
#  make && \
#  make install && \
#  checkinstall -y


RUN \
  git clone https://github.com/kvspb/nginx-auth-ldap /nginx-auth-ldap

RUN \
  apt-get install libldap-dev && \
  wget https://nginx.org/download/nginx-1.24.0.tar.gz && \
  tar zxf nginx-1.24.0.tar.gz && \
  cd nginx-1.24.0 && \
  ./configure --add-module=/nginx-auth-ldap/ && \
  make && \
  make install && \
  checkinstall -y

FROM ${IMAGE}

RUN \
  apt-get update -q && \
  apt-get install -yq libldap-2.4-2 ca-certificates gettext && \
  apt-get clean -yq && \
  mkdir -p /usr/local/nginx/conf/ /usr/local/nginx/logs/ /var/log/nginx/ && \
  ln -sf /dev/stdout /var/log/nginx/access.log && \
  ln -sf /dev/stderr /var/log/nginx/error.log && \
  ln -s /usr/local/lib/libpcre.so.1 /lib/libpcre.so.1

#COPY --from=0 /openssl-1.1.1c/openssl_1.1.1c-1_amd64.deb /tmp/
COPY --from=0 /zlib-1.2.11/zlib_1.2.11-1_amd64.deb /tmp/
COPY --from=0 /pcre-8.43/pcre_8.43-1_amd64.deb /tmp/
COPY --from=0 /nginx-1.24.0/nginx_1.24.0-1_amd64.deb /tmp/

RUN \
  dpkg -x /tmp/zlib_1.2.11-1_amd64.deb / && \
  dpkg -x /tmp/pcre_8.43-1_amd64.deb / && \
  dpkg -x /tmp/nginx_1.24.0-1_amd64.deb /
# && \
#  dpkg -x /tmp/openssl_1.1.1c-1_amd64.deb /

ADD run.sh mime.types nginx.conf.tpl /usr/local/nginx/conf/

STOPSIGNAL SIGTERM
EXPOSE 80
CMD /usr/local/nginx/conf/run.sh


