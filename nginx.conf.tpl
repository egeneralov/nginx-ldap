worker_processes 1;
error_log /var/log/nginx/error.log warn;
pid /var/run/nginx.pid;


events {
  worker_connections  1024;
}


http {
  include mime.types;
  default_type application/octet-stream;

  access_log /var/log/nginx/access.log;

  sendfile on;
  tcp_nopush on;

  keepalive_timeout  65;

  ldap_server ldap1 {
    url "$LDAP_URL";
    binddn "$BIND_DN";
    binddn_passwd $BIND_PASSWORD;
    group_attribute $GROUP_ATTRIBUTE;
    group_attribute_is_dn $group_attribute_is_dn;
    require valid_user;
  }

  server {
    listen 80;
    auth_ldap "$AUTH_MESSAGE";
    auth_ldap_servers ldap1;
    location / { proxy_pass $PROXY_TO; }
  }
}

