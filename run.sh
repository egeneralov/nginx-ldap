#!/bin/bash -xe

cat /usr/local/nginx/conf/nginx.conf.tpl | envsubst > /usr/local/nginx/conf/nginx.conf

cat /usr/local/nginx/conf/nginx.conf

/usr/local/nginx/sbin/nginx -g 'daemon off;'
