# nginx-ldap

Inspired by [nginx-auth-ldap](https://github.com/kvspb/nginx-auth-ldap) with think about RBAC.

## Warining

- Builded without ssl support (ldaps protocol also unavailable). For use only in trusted networks.
- After start nginx config will be available in logs. Be carefull, plaintext password will available.

## Environment

- **LDAP_URL**: `ldap://ldap.example.com:389/dc=example,dc=com?uid?sub?(memberOf=cn=admins,ou=groups,dc=example,dc=com)`
- **BIND_DN**: `cn=k8s-consul-ingress,ou=services,dc=example,dc=com`
- **BIND_PASSWORD**: `service-user-password-here`
- **GROUP_ATTRIBUTE**: `memberOf` for [FreeIPA](https://www.freeipa.org) or `sAMAccountName` for [Active directory](https://ru.wikipedia.org/wiki/Active_Directory)
- **group_attribute_is_dn**: `on`
- **AUTH_MESSAGE**: `admin power required`
- **PROXY_TO**: `http://consul-consul-ui.consul.svc.cluster.local`

## helm chart usage

    mkdir -p .helm
    wget -O artifacts.zip https://gitlab.com/egeneralov/nginx-ldap/-/jobs/artifacts/0.0.2/download?job=build-helm
    unzip artifacts.zip
    tar xzvf nginx-ldap-0.0.2.tgz -C .helm
    rm artifacts.zip nginx-ldap-0.0.2.tgz
    cp .helm/nginx-ldap/values.yaml values-nginx-ldap-consul.yaml
    ${EDITOR:-nano} values-nginx-ldap-consul.yaml
    OPTS=nginx-ldap-consul .helm/nginx-ldap -f values-nginx-ldap-consul.yaml --namespace consul --wait
    helm install --name ${OPTS} || helm upgrade ${OPTS}

## Feature Request

Just open an issue or send an email to [eduard@generalov.net](mailto:eduard@generalov.net)
